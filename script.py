import pandas as pd

# Read the Excel file
df = pd.read_excel('./patchesTest.xlsx')

# Assuming KB numbers are in a column named 'KB'
kb_numbers = df['KB ID'].tolist()

# Format and write to inventory.ini
with open('inventory.ini', 'w') as f:
    for kb in kb_numbers:
        f.write(f'kb_number={kb}\n')  # Adjust the format as needed for your inventory.ini